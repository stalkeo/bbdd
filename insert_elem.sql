
-- ----------------------- USERS --------------------
insert into usuarios values ('100315075@alumnos.uc3m.es', 'root', 'Mario', 'Sanchez', 'Garcia', 'Madrid', 28500, 1);
insert into usuarios values ('100317131@alumnos.uc3m.es', 'root', 'Juan Alonso', 'Machuca', 'Gonzalez', 'Madrid', 28500, 1);
insert into usuarios values ('100317181@alumnos.uc3m.es', 'root', 'Luis Alberto', 'Pantaleon', 'del Puerto', 'Madrid', 28500, 1);
insert into usuarios values ('100315630@alumnos.uc3m.es', 'root', 'Jose Luis', 'Parra', 'Olmedilla', 'Madrid', 28500, 1);

insert into usuarios values ('cr7@realmadrid.es', 'user', 'Cristiano', 'Ronaldo', null, 'Madrid', 28030, 0);
insert into usuarios values ('james@realmadrid.es', 'user', 'James', 'Rodriguez', null, 'Madrid', 28030, 0);
insert into usuarios values ('g_bale@realmadrid.es', 'user', 'Gareth', 'Bale', null, 'Madrid', 28030, 0);
insert into usuarios values ('k_benzema@realmadrid.es', 'user', 'Karim', 'Benzema', null, 'Madrid', 28030, 0);
insert into usuarios values ('a_morata@realmadrid.es', 'user', 'Alvaro', 'Morata', null, 'Madrid', 28030, 0);
insert into usuarios values ('s_ramos@realmadrid.es', 'user', 'Sergio', 'Ramos', null, 'Madrid', 28030, 0);	
insert into usuarios values ('isco@realmadrid.es', 'user', 'Isco', 'Alarcon', null, 'Madrid', 28030, 0);	

insert into usuarios values ('a@a', 'a', 'a', 'is', 'good', 'Madrid', 666, 1);
insert into usuarios values ('u@u', 'u', 'u', 'is', 'bad', 'Barcelona', 333, 0);	


-- ----------------------- PRODs --------------------
-- insert into productos values ('Intel Core i7', 1, 'Informatica', 0, 'u@u', 213.95, 0, 'Procesador de ultima generacion.');
-- insert into productos values ('Intel Core i7', 1, 'Informatica', 0, 'u@u', 213.95, 0, 'Procesador de ultima generacion.');
-- insert into productos values ('Intel Core i3', 1, 'Informatica', 0, 'u@u', 213.95, 0, 'Procesador de tercera generacion.');
-- insert into productos values ('Casa', 1, 'Vivienda', 0, 'u@u', 95200.00, 0, 'Casa de campo para escapadas.');
-- insert into productos values ('Gorra', 1, 'Moda', 0, 'u@u', 12.50, 0, 'Gorra con mucho estilo.');
-- insert into productos values ('Camiseta del Real Madrid', 1, 'Moda', 0, 'cr7@realmadrid.es', 59.99, 0, 'Vendo mi camiseta del ultimo partido. Esta un poco sudada, nada grave.');	


-- -- insert into imagenes values (1, 1, '|  |');

-- ----------------------- IMGS --------------------

-- insert into imagenes values (1, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (1, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (1, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (1, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));

-- insert into imagenes values (2, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (2, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (2, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));

-- insert into imagenes values (3, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));
-- insert into imagenes values (3, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\intel.jpg'));

-- insert into imagenes values (4, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\casa.jpg'));

-- insert into imagenes values (5, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\gorra.jpg'));

-- insert into imagenes values (6, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\camicr7.jpg'));
-- insert into imagenes values (6, 1, LOAD_FILE('F:\\Workspace\\GIT\\bbdd\\camicr7.jpg'));


