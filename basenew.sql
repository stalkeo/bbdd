DROP TABLE IF EXISTS imagenes;
DROP TABLE IF EXISTS productos;
DROP TABLE IF EXISTS usuarios;

CREATE TABLE usuarios(
	e_mail VARCHAR(50),
	password VARCHAR(50) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	apellido1 VARCHAR(50) NOT NULL,
	apellido2 VARCHAR(50),
	ciudad VARCHAR(50) NOT NULL,
	cod_postal INT(5) NOT NULL,
	administrador BOOLEAN NOT NULL,
	CONSTRAINT pk_usuarios1 PRIMARY KEY (e_mail),	
	CONSTRAINT chk_usuarios CHECK (cod_postal >=0)
);

CREATE TABLE productos(
	titulo VARCHAR(50) NOT NULL,
	id INT(10) NOT NULL,
	categoria VARCHAR(50) NOT NULL,
	num_img INT(1) NOT NULL,
	propietario VARCHAR(50) NOT NULL,
	precio FLOAT(8,2) NOT NULL,
	estado INT(1) NOT NULL, # 0 == Disponible, 1 == Vendido, 2 == Reservado
	descripcion VARCHAR(500) NOT NULL,
	CONSTRAINT pk_Producto PRIMARY KEY (id),
	CONSTRAINT fk_Prod1 FOREIGN KEY (propietario) REFERENCES usuarios (e_mail) ON DELETE CASCADE,
	CONSTRAINT chk_usuarios1 CHECK (estado = 0 OR estado = 1 OR estado = 2),
	CONSTRAINT chk_usuarios2 CHECK (num_img <= 4 AND num_img > 0),
	CONSTRAINT chk_usuarios3 CHECK (id >= 0),
	CONSTRAINT chk_usuarios4 CHECK (precio >= 0)
);

CREATE TABLE imagenes(
	id_prod INT(10) NOT NULL,
    num_img INT(1) NOT NULL,
	imagen MEDIUMBLOB NOT NULL,
	CONSTRAINT pk_imagenes PRIMARY KEY (id_prod, num_img),
	CONSTRAINT fk_Ventas1 FOREIGN KEY (id_prod) REFERENCES productos (id) ON DELETE CASCADE
);


drop trigger if exists img_met;
DELIMITER //
CREATE TRIGGER img_met BEFORE INSERT ON imagenes
FOR EACH ROW
BEGIN
DECLARE numero_imagenes INT (1);
DECLARE id_i INT (10);
SELECT num_img INTO numero_imagenes FROM productos WHERE id=NEW.id_prod;
  IF numero_imagenes  < 4 THEN
  UPDATE productos 
  SET num_img = numero_imagenes + 1 
  WHERE id = NEW.id_prod;
  ELSE
  signal sqlstate '45000' set message_text = 'El número máximo de imágenes es 4';
  END IF;

SELECT num_img INTO id_i FROM imagenes WHERE id_prod=NEW.id_prod ORDER BY num_img DESC limit 1;
IF id_i >= 0 THEN
  SET NEW.num_img = id_i + 1;
  
ELSE
   SET NEW.num_img = 0;
END IF;
  END //

drop trigger if exists id_productos;
DELIMITER //
CREATE TRIGGER id_productos BEFORE INSERT ON productos
FOR EACH ROW
BEGIN
DECLARE id_p INT (10);
SELECT id INTO id_p FROM productos ORDER BY id DESC limit 1;
IF id_p >= 1 THEN
  SET NEW.id = id_p + 1;
  SET NEW.num_img=0;
ELSE
   SET NEW.id = 1;
   SET NEW.num_img=0;
END IF;
  END//

