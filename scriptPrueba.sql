USE db_tiw;

SHOW TABLES;

DROP TABLE Productos;
DROP TABLE Usuarios;
DROP TABLE Ventas;

INSERT INTO Usuarios VALUES ('prueba@prueba.com', 'croquetitas', 'Luis', 'Pantaleon', 'Del Puerto', 'Madrid', 28030, false);
INSERT INTO Productos VALUES ('Movil BQ Aquaris M5', -50, 'Tecnología', 1, 'prueba@prueba.com', 5.3, 0);
INSERT INTO Productos VALUES ('Movil BQ Aquaris M5', 1, 'Tecnología', 10, 'prueba@prueba.com', 5.3, 5);


SELECT * FROM Imagenes;

SHOW CREATE TABLE Productos;

drop trigger if exists img_met;
DELIMITER //
CREATE TRIGGER img_met BEFORE INSERT ON Imagenes
FOR EACH ROW
BEGIN
DECLARE numero_imagenes INT (1);
SELECT num_img INTO numero_imagenes FROM Productos WHERE id=NEW.id_prod;
  IF numero_imagenes  < 4 THEN
  UPDATE Productos 
  SET num_img = numero_imagenes + 1 
  WHERE id = NEW.id_prod;
  ELSE
  signal sqlstate '45000' set message_text = 'My Error Message';
  END IF;
  END //
