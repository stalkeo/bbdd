DROP TABLE IF EXISTS Ventas;
DROP TABLE IF EXISTS Productos;
DROP TABLE IF EXISTS Usuarios;


CREATE TABLE Usuarios(
	e_mail VARCHAR(50),
	password VARCHAR(50) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	apellido1 VARCHAR(50) NOT NULL,
	apellido2 VARCHAR(50),
	ciudad VARCHAR(50) NOT NULL,
	administrador BOOLEAN NOT NULL,
	CONSTRAINT pk_Usuarios1 PRIMARY KEY (e_mail)
);

CREATE TABLE Productos(
	titulo VARCHAR(50) NOT NULL,
	id INT(10) NOT NULL,
	categoria VARCHAR(50) NOT NULL,
	num_img TINYINT(1) NOT NULL,
	propietario VARCHAR(50) NOT NULL,
	precio DECIMAL(8,2) NOT NULL,
	estado TINYINT(1) NOT NULL, # 0 == Disponible, 1 == Vendido, 2 == Reservado
	CONSTRAINT pk_Producto PRIMARY KEY (id),
	CONSTRAINT fk_Prod1 FOREIGN KEY (propietario) REFERENCES Usuarios (e_mail),
	CONSTRAINT chk_Usuarios1 CHECK (estado = 0 OR estado = 1 OR estado = 2),
